import enum

class PointType(enum.Enum):
    CO_SO_CACH_LY_TAP_TRUNG = 1
    KHU_VUC_CACH_LY = 2
    NHA_BENH_NHAN = 3
    BN_DEN_NHO_HON_14 = 4
    CHOT_KIEM_DICH = 15
    BN_DEN_HON_14 = 6
    DIEM_TIEM_CHUNG = 19