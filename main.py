import settings
from app import app
from registerRoute import register_route

if __name__ == "__main__":
    register_route(app)
    app.run(host=settings.HOST, port=settings.PORT, debug=True)