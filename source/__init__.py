from bson import json_util
from bson import ObjectId
from flask import Flask, Response
from flask_pymongo import PyMongo
import codecs
app = Flask(__name__)

app.config['MONGO_URI'] = "mongodb://localhost:27017/admin"
mongo = PyMongo(app)

@app.route('/users', methods=['GET'])
def get_users():
    users = mongo.db.datacovid.find()
    response = json_util.dumps(users,ensure_ascii=False).encode('utf8')
    return Response(response.decode(), mimetype="application/json")

@app.route('/users/<id>', methods=['GET'])
def get_user(id):
    print(id)
    user = mongo.db.datacovid.find_one({'_id': ObjectId(id),})
    response = json_util.dumps(user,ensure_ascii=False).encode('utf8')
    return Response(response.decode(), mimetype="application/json")

if __name__=="__main__":
    app.run(debug=True)